FROM openjdk:8-alpine

WORKDIR /app

COPY ./build/libs/best-path-*.jar /app/best-path.jar

ENTRYPOINT ["java", "-jar", "/app/best-path.jar"]

CMD [""]