# Best Path
> Calcula o menor caminho em um grafo direcionado.

Calcula o caminho com menor custo em um grafo direcionado.
Formato: _A -> B (10)_
Onde temos *A* e *B* representando nós e *10* o custo entre eles.

## Instalação
Compilando o projeto
```sh
./gradlew clean && ./gradlew build
```

Compilando o projeto e gerando uma imagem docker
```sh
./gradlew clean && ./gradlew build && docker build --no-cache -t crisun/best-path .
```

Compilando o projeto e gerando uma imagem docker com o docker-compose
```sh
./gradlew clean && ./gradlew build && docker-compose build
```

## Exemplo de uso

Calculando o caminho de um grafo
```sh
 docker run -it --rm crisun/best-path --no "A->F" --grafo "A->B(5)B->C(4)C->D(3)D->B(2)C->E(6)E->C(6)D->F(1)"
 A
 B
 C
 D
 F

 docker run -it --rm crisun/best-path --no "A->F" --grafo "A->B(12)A->C(23)B->D(11)B->F(14)C->E(11)D->E(2)D->F(1)F->E(6)"
 A
 B
 D
 F
```

## Bibliotecas utilizadas
Clikt: [https://github.com/ajalt/clikt]

Algoritmo de Dijkstra: [https://www.ime.usp.br/~pf/algoritmos_para_grafos/aulas/dijkstra.html]