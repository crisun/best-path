package br.com.crisun.bestpath

import br.com.crisun.bestpath.algorithm.DijkstraAlgorithm
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.options.validate

class BestPath : CliktCommand() {
    private val parser = Parser()
    private val validator = Validator()

    var path: List<String> = emptyList()

    val graph by option("-g", "--grafo", help = "Grafo direcionado A -> B (5)").required().validate {
        require(validator.isGraphValid(it)) {
            "Formato do grafo: A -> B (5) B -> C(3)..."
        }
    }

    val nodes by option("-n", "--no", help = "No de origem e no de destino A -> B").required().validate {
        require(validator.isPathValid(it)) {
            "Formato dos no: A -> B"
        }
    }

    override fun run() {
        val nodes = parser.nodes(nodes)

        path = DijkstraAlgorithm(parser.graph(graph)).run {
            execute(nodes.first)
            getPath(nodes.second)
        }

        for (node in path) {
            println(node)
        }
    }
}

fun main(args: Array<String>) = BestPath().main(args)
