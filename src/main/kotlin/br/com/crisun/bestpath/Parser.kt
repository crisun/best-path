package br.com.crisun.bestpath


import br.com.crisun.bestpath.model.Node
import br.com.crisun.bestpath.model.Graph

val NAME_REGEX = Regex("[a-zA-Z0-9 ]+")
val VERTEX_REGEX = Regex("[->]{2}")
val DISTANCE_REGEX = Regex("\\d+")
val PATH_REGEX = Regex("^[a-zA-Z0-9 ]+[->]{2}[a-zA-Z0-9 ]+\$")
val GRAPH_REGEX = Regex("[a-zA-Z0-9 ]+[->]{2}[a-zA-Z0-9 ]+\\(\\d+\\)")

class Parser {
    private fun elements(nodes: String) = GRAPH_REGEX.findAll(nodes.replace(" ", ""))

    private fun distance(nodes: String): Int {
        val split = VERTEX_REGEX.split(nodes)
        return DISTANCE_REGEX.find(split[1])!!.value.toInt()
    }

    private fun extractNodes(nodes: String): Pair<String, String> {
        val split = VERTEX_REGEX.split(nodes)
        return Pair(split[0], NAME_REGEX.find(split[1])!!.value)
    }

    fun graph(input: String): Graph {
        val result = mutableListOf<Node>()
        val elements = elements(input)

        elements.forEach {
            val distance = distance(it.groups[0]!!.value)

            extractNodes(it.groups[0]!!.value).run {
                result.add(Node(first, second, distance))
            }
        }

        return Graph(result)
    }

    fun nodes(nodes: String): Pair<String, String> {
        val split = VERTEX_REGEX.split(nodes)
        return Pair(split[0], split[1])
    }
}