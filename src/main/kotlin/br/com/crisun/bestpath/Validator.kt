package br.com.crisun.bestpath

class Validator {
    fun isGraphValid(args: String) = GRAPH_REGEX.findAll(args.replace(" ", "")).count() > 1

    fun isPathValid(args: String) = PATH_REGEX.matches(args.replace(" ", ""))
}