package br.com.crisun.bestpath.algorithm

import br.com.crisun.bestpath.model.Graph
import java.util.*
import kotlin.collections.HashSet

class DijkstraAlgorithm(graph: Graph) {
    private val nodes = ArrayList(graph.nodes)
    private val unSettledNodes = HashSet<String>()
    private var settledNodes: MutableSet<String> = HashSet()
    private var distance: MutableMap<String, Int> = HashMap()
    private var predecessors: MutableMap<String, String> = HashMap()

    fun execute(source: String) {
        distance[source] = 0
        unSettledNodes.add(source)
        while (unSettledNodes.size > 0) {
            val node = getMinimum(unSettledNodes)
            settledNodes.add(node)
            unSettledNodes.remove(node)
            findMinimalDistances(node)
        }
    }

    fun getPath(target: String): LinkedList<String> {
        val path = LinkedList<String>()
        var step = target

        if (predecessors[step] == null) {
            return LinkedList()
        }

        path.add(step)
        while (predecessors[step] != null) {
            step = predecessors[step]!!
            path.add(step)
        }
        path.reverse()

        return path
    }

    private fun findMinimalDistances(node: String) {
        val adjacentNodes = getNeighbors(node)
        for (target in adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node) + getDistance(node, target)) {
                distance[target] = getShortestDistance(node) + getDistance(node, target)
                predecessors[target] = node
                unSettledNodes.add(target)
            }
        }
    }

    private fun getMinimum(nodes: Set<String>): String {
        var minimum: String = nodes.first()

        for (node in nodes) {
            if (getShortestDistance(node) < getShortestDistance(minimum)) {
                minimum = node
            }
        }
        return minimum
    }

    private fun getNeighbors(target: String): List<String> {
        val neighbors = ArrayList<String>()
        for (node in nodes) {
            if (node.source == target && !settledNodes.contains(node.destination)) {
                neighbors.add(node.destination)
            }
        }
        return neighbors
    }

    private fun getShortestDistance(destination: String): Int {
        return distance[destination] ?: Integer.MAX_VALUE
    }

    private fun getDistance(source: String, destination: String): Int {
        for (node in nodes) {
            if (node.source == source && node.destination == destination) {
                return node.distance
            }
        }
        throw RuntimeException("Should not happen")
    }
}