package br.com.crisun.bestpath.model

data class Graph(val nodes: List<Node>)