package br.com.crisun.bestpath.model

data class Node(val source: String, val destination: String, val distance: Int) {
}