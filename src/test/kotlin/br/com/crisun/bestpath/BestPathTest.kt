package br.com.crisun.bestpath

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class BestPathTest {
    val bestPath = BestPath()

    @Test
    fun `Menor caminho do grafo 1`() {
        val nodes = "A->F"
        val graph = "A -> B (5)B -> C (4)C -> D (3)D -> B (2)C -> E (6)E -> C (6)D -> F (1)"

        bestPath.main(listOf("-n$nodes", "-g$graph"))

        Assertions.assertEquals(graph, bestPath.graph)
        Assertions.assertEquals(nodes, bestPath.nodes)
        Assertions.assertEquals(listOf("A", "B", "C", "D", "F"), bestPath.path)
    }

    @Test
    fun `Menor caminho do grafo 2`() {
        val nodes = "B->E"
        val graph = "A -> B (5)B -> C (4)C -> D (3)D -> B (2)C -> E (6)E -> C (6)D -> F (1)"

        bestPath.main(listOf("-n$nodes", "-g$graph"))

        Assertions.assertEquals(graph, bestPath.graph)
        Assertions.assertEquals(nodes, bestPath.nodes)
        Assertions.assertEquals(listOf("B", "C", "E"), bestPath.path)
    }
}