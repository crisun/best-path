package br.com.crisun.bestpath

import br.com.crisun.bestpath.model.Graph
import br.com.crisun.bestpath.model.Node
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ParserTest {
    val parser = Parser()

    @Test
    fun `Parser de um grafo com um no`() {
        val graph = parser.graph("A->B(10)")
        val expectedGraph = Graph(listOf(Node("A", "B", 10)))

        Assertions.assertEquals(expectedGraph, graph)
    }

    @Test
    fun `Parser de um grafo com mais de um no`() {
        val graph = parser.graph("A->B(10) B->C(5)")
        val expectedGraph = Graph(listOf(Node("A", "B", 10), Node("B", "C", 5)))

        Assertions.assertEquals(expectedGraph, graph)
    }

    @Test
    fun `Parser de um grafo com nos espacados`() {
        val graph = parser.graph("A -> B( 10)  B -> C( 5)")
        val expectedGraph = Graph(listOf(Node("A", "B", 10), Node("B", "C", 5)))

        Assertions.assertEquals(expectedGraph, graph)
    }

    @Test
    fun `Parser de um grafo com distancia invalida`() {
        val graph = parser.graph("A -> B( 10)  ")
        val expectedGraph = Graph(listOf(Node("A", "B", 10)))

        Assertions.assertEquals(expectedGraph, graph)
    }
}