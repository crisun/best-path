package br.com.crisun.bestpath

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ValidatorTest {
    val validator = Validator()

    @Test
    fun `Grafo valido`() {
        assertEquals(true, validator.isGraphValid("A->B(10)A->B(10)"))
        assertEquals(true, validator.isGraphValid("A->B(10)A->C(5)B->C(1)"))
    }

    @Test
    fun `Grafo invalido`() {
        assertEquals(false, validator.isGraphValid("A->B(10)"))
        assertEquals(false, validator.isGraphValid("A->B(10)A->B(a10)"))
        assertEquals(false, validator.isGraphValid("A->B(10)A->B10"))
    }

}